#Implementation

Include the file in your project folder. 

##Installing connect.js

`npm install connect serve-static`

##Go to localhost:3000

Change host address in server.js if neccessary

##Alternative

You can also use pytho's simpleHTTPServer, if you have python installed.

`python -m SimpleHTTPServer 8080`

You can also use NodeJs with [http-Server] (https://github.com/indexzero/http-server). 

`npm install http-server -g`


`http-server [path] [options]`


example `http-server -p`